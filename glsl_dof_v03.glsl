#version 120

#define USE_PSF

#define FOCUS 2
#define APERTURE 1.1
#define RADIUS 40
#define SPLATRADIUS 100
#define NUMSAMPLES 100
#define FOCUS_RANGE 3.0

#define BIG_FLOAT 9999999.
#define SMOL_FLOAT 0.0000001
#define PI 3.1415926
#define TAU 6.2831853
#define AA 1

// Set up uniforms for accessing input texture and resolution
uniform vec2 iResolution;
uniform sampler2D iTexture;


float CircleOfConfusion(float depth, float focus)
{
    float coc = abs((depth - focus) / FOCUS_RANGE);
    if (coc > 1.0)
        coc = coc;
    return coc;
}


float cocFraction(float coc)
{
    float fr = 1.0/(RADIUS*RADIUS*(3.14*(coc*coc)));
    return fr;
}


vec4 inUnitCircle(vec2 normalizedCoord)
{
    vec2 psfUV = 0.5*(normalizedCoord+RADIUS)/RADIUS;
    vec2 UV = 0.5*(normalizedCoord+1.0);
    vec4 psf = texture2D(iTexture3, UV);
    return psf;
}


// Calculate cats eye occlusion from normalized coordinate within unit circle and absolute sample coordinate in image
vec4 catsEye(vec2 samplePos, vec2 uv)
{
    vec2 shift = uv*2.0 - vec2(1.0);
    shift*=0.81;
    vec4 cat = vec4(inUnitCircle(samplePos+shift)*inUnitCircle(samplePos));
    return cat;
}


// Apply pixel spread function to sample position
// Sample pos is expressed in  -RADIUS to +RADIUS range
vec4 pixelSpread(vec2 samplePos)
{
    vec2 psfUV = 0.5*(2.0*samplePos+RADIUS)/RADIUS;
    psfUV = ((2.0*samplePos / float(RADIUS))+1.0)*0.5;
    vec4 psf = texture2D(iTexture2, psfUV);
    psf = psf * catsEye(samplePos/RADIUS, gl_FragCoord.xy / iResolution.xy);
    return psf;
}


// Function that calculates distance from line specified by points p1 and p2
// This is used to check if point is within specific radius from moving defocused point
float pDistance(vec2 pos, vec2 p1, vec2 p2)
{
  float A = pos.x - p1.x;
  float B = pos.y - p1.y;
  float C = p2.x - p1.x;
  float D = p2.y - p1.y;

  float dot = A * C + B * D;
  float len_sq = C * C + D * D;
  float param = -1.0;
  if (len_sq != 0) //in case of 0 length line
      param = dot / len_sq;

  float xx;
  float yy;

  if (param < 0) {
    xx = p1.x;
    yy = p1.y;
  }
  else if (param > 1) {
    xx = p2.x;
    yy = p2.y;
  }
  else {
    xx = p1.x + param * C;
    yy = p1.y + param * D;
  }

  float dx = pos.x - xx;
  float dy = pos.y - yy;
  return sqrt(dx * dx + dy * dy);
}


// Check if point is within motion blurred point spread
float inMotionBlurredCoC(vec2 originalPos, vec2 samplePos, vec2 sampleMotion, float splatRadius)
{
    float result = 0.0;
    float distToLine = 0.0;
    vec2 s1 = samplePos + sampleMotion*0.5;
    vec2 s2 = samplePos - sampleMotion*0.5;
    distToLine = pDistance(originalPos, s1, s2);
    if (distToLine < splatRadius)
        result = 1.0;
    return result;
}


vec4 render(vec2 fragCoord)
{
    // Calculate normalized UV coordinates for source image from frag coords and resolution
    vec2 UV = gl_FragCoord.xy / iResolution.xy;

    // Read original image color for fragment from input image
    vec4 originalColor = texture2D(iTexture0, UV);

    // Read motion vectors and depth value from data input
    vec2 mv = texture2D(iTexture1, UV).xy;
    float depth= texture2D(iTexture1, UV).b;

    // Calculate circle of confusion from depth
    float coc = CircleOfConfusion(depth, FOCUS);

    // Normalized maximum search radius in input image, divide pixel size RADIUS with resolution
    float nRadius = RADIUS/iResolution.x;
    float sRadius = SPLATRADIUS/iResolution.x;

    float sCount = 0.0;
    vec4 col = vec4(0.0);

    // Accumulation loop
    // // Here it would actually be better to substitute with random sampling, so arbitrary size could be sampled with less computation
    for (int yc = -RADIUS; yc <= RADIUS; yc++)
    {
        for (int xc = -RADIUS; xc <= RADIUS; xc++)
        {
            // Relative sample position in normalized coords
            vec2 samplePos = vec2(xc/iResolution.x, yc/iResolution.x);

            // Absolute sample position in normalized coords
            vec2 sc = UV + samplePos;

            vec4 smplData = texture2D(iTexture1, sc);
            float cocData = CircleOfConfusion(smplData.b, FOCUS);
            vec2 samplemv = smplData.rg;
            vec4 smplColor = texture2D(iTexture0, sc);

            // If defined, look up spread color from kernel. Does not work with mblur currently
            #ifdef USE_PSF
            smplColor *= pixelSpread(vec2(xc, yc));
            #endif

            float accumulate = 0.0;
            if (inMotionBlurredCoC(UV, sc, samplemv, nRadius*cocData) > 0.0)
            {
                accumulate = 1.0;
            }

            accumulate = accumulate*cocFraction(cocData);
            col += smplColor * accumulate;

            sCount+=accumulate;
        }
    }

    return col/sCount;
}

void main()
{
    vec4 fColor = render(gl_FragCoord.xy);
    gl_FragColor = fColor ;
};
