# Unified lens effects

Tests with unified motion blur and dof handling.

Objective is to create a filter which does motion blur and dof in one single pass. Current version is written in glsl for use in NukeGLSL renderer I wrote for Nuke. That plugin can be found here: https://gitlab.com/hendrikproosa/nuke-glsl-shader/

It is pretty buggy and glitches but general idea seems to work. It is basically a gathering based method where I check every pixel within specified radius and if it should contribute to current pixel, its contribution is weighted and added. The weighting part is not great and produces errors in focused areas.

Demo of some features can be seen here:
https://vimeo.com/427725312